# Presentation

Repository for the presentation "Power Up Node.js with C++".

Skeleton for presentation:

* Title

* #whoami

* Why Native Modules?

* Why C++?

* Optional - why not rust/C#...
   
* Native Modules.
    
* API VS ABI

* Native Modules - Types.

* V8

* Using V8

* NAN

* Using NAN

* N-API

* Using N-API

* Benchmarks

* Deployment

* Tools
